import "dotenv/config";
import express from "express";
import http from "node:http";
import session from "express-session";
import "reflect-metadata";
import Routes from "./routes";
import * as crypto from "crypto";
import startDb from "./config/db";
import { buildSchema } from "type-graphql";
import { ApolloServer } from "apollo-server-express";
import { resolvers } from "./graphql-entry/graphql.resolvers";
import Container from "typedi";

async function main() {
  const app = express();
  const httpServer = http.createServer(app);
  const SESSION_SECRET = process.env.SESSION_SECRET as string;
  startDb();
  app.use(
    session({
      genid: () => crypto.randomUUID(),
      secret: SESSION_SECRET,
      resave: false,
      saveUninitialized: false,
    })
  );

  Routes(app);

  const schema = await buildSchema({
    resolvers,
    nullableByDefault: true,
    container: Container,
  });

  const apolloServer = new ApolloServer({
    schema,
  });

  await apolloServer.start();
  apolloServer.applyMiddleware({ app, path: "/graphql" });

  await new Promise<void>((resolve) =>
    httpServer.listen({ port: process.env.PORT }, resolve)
  );
  console.log(`🚀 Server started on http://localhost:${process.env.PORT}`);
}

main();
