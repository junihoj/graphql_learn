import express, { Request, Response } from 'express';
import {Router} from 'express-serve-static-core';
import userRoute from '../users/routes';
const router = express.Router();
router.get("/", function(req:Request, res:Response){
    req.query;
    res.send("WELCOME HERE")
})
export default function  Routes(app: {
    use: (path: string, expressRoute: Router) => void;
  }){
    app.use('/',router )
    app.use("/user", userRoute)
}