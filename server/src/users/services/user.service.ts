import { Service } from "typedi";
import UserModel from "../models/user.model";
import Response from "../../utils/responses";

@Service()
class UserService {
  async getAllUser() {
    const users = await UserModel.find({});
    if (users) {
      return new Response(200, true, "done", users);
    }
    return new Response(200, true, "done", []);
  }
}

export default UserService;
