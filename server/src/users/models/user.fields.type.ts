import { ObjectType, Field } from "type-graphql";
import { prop, modelOptions, Severity } from "@typegoose/typegoose";

@ObjectType()
@modelOptions({ options: { allowMixed: Severity.ALLOW } })
export class Account {
  @Field()
  @prop()
  account_number: string;

  @Field()
  @prop()
  account_type?: string;
}
