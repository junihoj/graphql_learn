import { ObjectType } from "type-graphql";
import { Field } from "type-graphql";
import { prop } from "@typegoose/typegoose";
import { Account } from "./user.fields.type";

@ObjectType()
class User {
  @Field()
  @prop()
  firstname?: string;

  @Field()
  @prop()
  lastname?: string;

  @Field()
  @prop()
  email?: string;

  @Field()
  @prop()
  password?: string;

  @Field(() => Account)
  @prop()
  account: Account;
}

export default User;
