import { ObjectType, Field } from "type-graphql";
import User from "../../models/user.schema";
import ResolverResponse from "../../../utils/resolver.response";

@ObjectType()
export class GetAllUser {
  @Field()
  statusCode: number;
  @Field()
  success: string;
  @Field()
  data: User;
}

@ObjectType()
export class UserResponse extends ResolverResponse {
  @Field(() => User)
  data: User;
}

@ObjectType()
export class GetAllUserResponse extends ResolverResponse {
  @Field(() => [User])
  data: User;
}
