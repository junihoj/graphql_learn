import { Resolver, Query, Mutation } from "type-graphql";
import { Service, Inject } from "typedi";
import UserService from "../services/user.service";
import { GetAllUserResponse } from "../dto/return-types/user.return.type";
// users/servicess/user.service
@Resolver()
@Service()
export class UserResolver {
  @Inject()
  userService: UserService;
  // constructor(
  //   private readonly userService: UserService // eslint-disable-next-line no-empty-function
  // ) {}

  @Query(() => GetAllUserResponse)
  async getAllUser() {
    return this.userService.getAllUser();
  }

  @Query(() => String)
  async sayHello() {
    return "Hello";
  }

  // @Mutation()
  // async createUser() {}
}
