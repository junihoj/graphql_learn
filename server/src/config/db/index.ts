import mongoose, { ConnectOptions } from "mongoose";
import 'dotenv/config'

const startDb = async ()=>{
    try{
        await mongoose.connect(process.env.MONGODB_URL as string, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
          } as ConnectOptions);
        console.log("successfully Connected to DB")
    }catch(err){
        console.log("MONGODB ERROR", err)
    }
}

export default startDb;