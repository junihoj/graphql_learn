import {ObjectType, Field} from 'type-graphql';

@ObjectType()
export default class ResolverResponse{
    @Field()
    statusCode: number;
    @Field()
    success: boolean;
    @Field()
    message: string;
}