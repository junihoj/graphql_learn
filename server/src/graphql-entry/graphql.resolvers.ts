import { UserResolver } from "../users/resolvers/user.resolver";

export const resolvers = [UserResolver] as const;
