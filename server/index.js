import express from "express";
import session from "express-session";
const PORT = 4000;
const app = express();
const SESSION_SECRET = process.env.SESSION_SECRET;
app.use(
  session({
    genid: (req) => crypto.randomUUID(),
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
  })
);

app.listen({ port: process.env.PORT }, () => {
  console.log(`🚀 Server ready at http://localhost:${PORT}`);
});
